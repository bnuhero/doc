Apache Mesos 发展简史
=====================

# 历程

2008年（？），加州伯克利大学 [AMPLab](https://amplab.cs.berkeley.edu/) 博士生 Benjamin Hindman 正在研究如何在 Intel 的多核 CPU 上有效地同时执行多个计算任务。他发现，每个应用程序都会贪心地占用尽可能多的计算资源，导致其它应用无法被执行。为此，需要设计一种系统：为每个计算任务分配固定数目的 CPU 核，根据任务的执行情况动态调整 CPU 多核的分配。

与此同时，他的两位博士同学兼朋友 Andy Konwinski 和 Matei Zaharia 正在研究如何在整个数据中心运行分布式系统，如 Hadoop 和 各种 NOSQL 数据库等。以前，一个计算机集群只运行一个分布式系统。如果要运行另外一个分布式系统，就需要为它单独构建一个新的计算机集群。既然多个单机计算任务可以有效地共享同一个多核 CPU ，那为什么多个分布式系统不能有效地共享同一个计算机集群呢？为此，三人开始合作开发一个名为 Nexus 的系统，这也是他们学习「计算机系统高级专题」课程的一个实践项目。

2009年， Andy Konwinski 在 HotCloud'09 会议上做了有关 Nexus 系统的报告，论文是 [A Common Substrate for Cluster Computing](http://usenix.org/event/hotcloud09/tech/full_papers/hindman.pdf)。

2010年，由于与另外一个校内研究项目重名，三人把系统改名为 Mesos 。Benjamin Hindman 等人去 Twitter 做报告，引起了几位工程师的兴趣。他们都在 Google 工作过，见识过 Borg 系统的威力，感觉可以利用 Mesos 重建一个类似的系统。于是， Twitter 邀请 Hindman 做顾问（后来是实习生），与这些工程师一起扩展 Mesos 项目。一年之后， Hindman 成为 Twitter 的全职员工。

*花边：这次交流有 8 位 Twitter 工程师参加。人这么少，Hindman 有些失望。但是 Twitter 首席科学家告诉他，百分之十的员工都来了。当时 Twitter 的规模还比较小。*

2011年，论文 [Mesos: A Platform for Fine-Grained Resource Sharing in the Data Center](http://people.csail.mit.edu/matei/papers/2011/nsdi_mesos.pdf) 发表， Matei Zaharia 在 NSDI'11 会议上做报告。

2012年， Mesos 已经在 Twitter 得到了广泛地应用。 Twitter 开始赞助 Apache 基金会， Mesos 成为 Apache 的一个孵化项目。当时，三个人都在 Twitter 的工程团队里。 AirBnB 也应用 Mesos 管理数据分析集群。

2013年， Apache Mesos 成为正式项目。 Mesosphere 公司成立，

# Mesos 与 Google 的关系

Google 赞助了 AMPLab ，它的员工也与 AMPLab 师生有很好地交流。Andy Konwinski 曾经做过 Google 实习生，在 Borg 团队成员手下工作过。虽然没有讨论底层技术细节，但在比较泛的层次上，讨论过问题有哪些，怎么看待这些问题。此外， 从事 Mesos 研发的 Twitter 工程师都在 Google 工作过，至少有过使用经验。

总的来说，二者只有一些间接的联系，并无直接关联。 Google 开源的相关项目是 Kubernetes 。

# Mesosphere

[Mesosphere](https://mesosphere.com/) 成立于 2013 年，主要产品是以 Apache Mesos 为基础的数据中心操作系统。

Benjamin Kindman 是创始人之一，但是目前好像没有担任正式职务。

Florian Leibert 是创始人兼 CEO ，他原先在 Twitter 工作，后来到 AiBnB 构建了数据分析和处理平台。他是常用的 Mesos 框架 Chronos 的开发者。

联合创始人、 CTO 是 Tobi Knaup ，他原先是 AirBnB 的技术负责人，也是最常用的 Mesos 框架 Marathon 的开发者。

公司经过三轮融资：

* 种子 230 万美金， 2013 年 5 月；
* A 轮 1050 万美金， 2014 年 6 月；
* B 轮 3600 万美金， 2014 年 12 月。

# 参考文章

1. [Return of the Borg: How Twitter Rebuilt Google's Secret Weapon](http://www.wired.com/2013/03/google-borg-twitter-mesos/), Wired, 2013.
2. 维基百科词条： [Apache Mesos](http://en.wikipedia.org/w/index.php?title=Apache_Mesos) 
