使用 CentOS 7 虚拟机集群搭建 Mesos 系统
=======================================

# VirtualBox

原计划使用 KVM 虚拟机集群，但是 virt-manager 不是很稳定（也许是我还不熟悉），所以切换到 VirtualBox 。

在“全局设定”菜单中，新建一个 NAT 网络，取名为 `NAT-Mesos-CentOS`：

+ 网络 CIDR: 10.0.2.0/24
+ 开启 DHCP

# Master 节点

## 安装

### 软件包方式

首先，添加软件源：

```console
sudo rpm -Uvh http://repos.mesosphere.io/el/7/noarch/RPMS/mesosphere-el-repo-7-1.noarch.rpm
```

其次，安装 mesos 和 marathon ：

```console
sudo yum install mesos marathon
```

最后，单独安装 ZooKeeper ：

```console
sudo yum install mesos-zookeeper
```

### 源码方式

## 配置

在配置之前，先停掉相关服务：

```console
sudo systemctl stop zookeeper.service
sudo systemctl stop mesos-master.service
sudo systemctl stop mesos-slave.service
sudo systemctl disable mesos-slave.service
sudo systemctl stop marathon.service
```

### 网络

#### [分配一个固定的 IP 地址](http://ask.xmodulo.com/configure-static-ip-address-centos7.html)

找出当前使用的网卡：`nmcli dev status`，通常是`enp0s3`。

执行`nmtui edit enp0s3`，修改操作如下所示：

+ IPv4 Configuration
    * Addresses: `10.0.2.16/24`
    * Gateway: `10.0.2.1`
    * DNS Servers: `10.0.2.2`和`202.116.32.4`
+ 开启`Require IPv4 addressing for this connection`

上述操作实际上修改的是`/etc/sysconfig/network-scripts/ifcfg-enp0s3`配置文件。

重新启动网络服务：`sudo systemctl restart network.service`

在本次实验中， 3 台 master 节点的 IP 地址分别是

+ `10.0.2.16`
+ `10.0.2.17`
+ `10.0.2.18`

#### 开启必要的网络端口

ZooKeeper 用到的端口：

```console
sudo firewall-cmd --add-port 2181/tcp --permanent
sudo firewall-cmd --add-port 2888/tcp --permanent
sudo firewall-cmd --add-port 3888/tcp --permanent
```

Mesos-master 用到的端口：

```console
sudo firewall-cmd --add-port 5050/tcp --permanent
```

Marathon 用到的端口：

```console
sudo firewall-cmd --add-port 8080/tcp --permanent
```

重新加载防火墙服务：

```console
sudo firewall-cmd --reload
```

查看目前开放的端口：

```console
sudo firewall-cmd --list-ports
```

### ZooKeeper

### 服务器序号

编辑`/var/lib/zookeeper/myid`， 3 台主节点的标识号（介于 1-255 之间的整数）分别设为：

+ `10.0.2.16` <-> `1`
+ `10.0.2.17` <-> `2`
+ `10.0.2.18` <-> `3`

### 服务器 IP 地址

编辑`/etc/zookeeper/conf/zoo.cfg`，输入如下的内容：

```conf
server.1=10.0.2.16:2888:3888
server.2=10.0.2.17:2888:3888
server.3=10.0.2.18:2888:3888
```

### Mesos

#### ZooKeeper 服务器地址

编辑`/etc/mesos/zk`，设置为

```conf
zk://10.0.2.16:2181,10.0.2.17:2181,10.0.2.18:2181/mesos
```

#### quorum

编辑`/etc/mesos-master/quorum`，设置为`2`

#### 主机名

编辑`/etc/mesos-master/hostname`，设置为该主机的 IP 地址。

#### mesos-master 服务监听的 IP 地址

编辑`/etc/default/mesos-master`，添加一行：

```conf
IP=10.0.2.x
```

#### 停掉 mesos-slave 服务

```console
sudo systemctl stop mesos-slave.service
sudo systemctl disable mesos-slave.service
```

# Slave 节点

## 安装

### 软件包方式

添加软件源：

```console
sudo rpm -Uvh http://repos.mesosphere.io/el/7/noarch/RPMS/mesosphere-el-repo-7-1.noarch.rpm
```

安装 mesos 软件包：

```console
sudo yum -y install mesos
```

### 源码方式

## 配置

在配置之前，先停掉相关的服务：

```console
sudo systemctl stop mesos-master.service
sudo systemctl disable mesos-master.service
sudo systemctl stop mesos-slave.service
```

### 网络

#### 固定 IP 地址

网络链接方式选择`NAT网络`。

找出当前使用的网卡：`nmcli dev status`，通常是`enp0s3`。

执行`nmtui edit enp0s3`，修改操作如下所示：

+ IPv4 Configuration
    * Addresses: `10.0.2.20/24`
    * Gateway: `10.0.2.1`
    * DNS Servers: `10.0.2.2`和`202.116.32.4`
+ 开启`Require IPv4 addressing for this connection`

上述操作实际上修改的是`/etc/sysconfig/network-scripts/ifcfg-enp0s3`配置文件。

重新启动网络服务：`sudo systemctl restart network.service`

#### 开启端口

开启`5051`端口。

```console
sudo firewalld-cmd --add-port 5051/tcp --permanent
sudo firewalld-cmd --reload
```

### Mesos

#### ZooKeeper 服务器地址

编辑`/etc/mesos/zk`，设置为

```conf
zk://10.0.2.16:2181,10.0.2.17:2181,10.0.2.18:2181/mesos
```

#### 主机名

编辑`/etc/mesos-slave/hostname`，设置为`10.0.2.20`。

#### mesos-slave 服务监听的 IP 地址

编辑`/etc/default/mesos-slave`，添加一行：

```conf
IP=10.0.2.20
```

# 参考文档

+ [Setting up a Mesosphere Cluster](https://docs.mesosphere.com/getting-started/datacenter/install/)
